# Función suma
def suma(a, b):
    return a + b


# Función resta
def resta(a, b):
    return a - b


if __name__ == '__main__':
    print(f'1 + 2 = {suma(1, 2)}')
    print(f'3 + 4 = {suma(3, 4)}')
    print(f'6 - 5 = {resta(6, 5)}')
    print(f'8 - 7 = {resta(8, 7)}')
